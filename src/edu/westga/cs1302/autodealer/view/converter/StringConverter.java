package edu.westga.cs1302.autodealer.view.converter;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import edu.westga.cs1302.autodealer.model.DealershipGroup;

/**
 * Converters Strings
 * 
 * @author jeremy.trimble
 *
 */
public class StringConverter {

	/**
	 * Formats the name of a dealership. Each word in the name changes to titlecase.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param name
	 *            the current name of the dealership.
	 * 
	 * @return the new name of the dealership, returns original name if ALL_AUTOS is
	 *         passed in.
	 */
	public static String formatString(String name) {
		if (!name.equals(DealershipGroup.ALL_AUTOS)) {
			name = name.toLowerCase();
			String[] words = name.split("\\s");
			String newName = "";
			words[0].trim();
			if (!words[0].equals("")) {
				char capitalFirstWord = Character.toUpperCase(words[0].charAt(0));
				newName += capitalFirstWord + words[0].substring(1) + " ";
			}

			for (int i = 1; i < words.length; i++) {
				words[i].trim();
				if (!isArticleOrPreposition(words[i])) {
					char capital = Character.toUpperCase(words[i].charAt(0));
					newName += capital + words[i].substring(1) + " ";
				} else {
					newName += words[i] + " ";
				}
			}
			return newName.trim();
		}
		return name;
	}

	private static boolean isArticleOrPreposition(String word) {
		String[] artsAndPreps = loadArtsAndPreps();

		for (String current : artsAndPreps) {
			if (word.equals(current)) {
				return true;
			}
		}
		return false;
	}

	private static String[] loadArtsAndPreps() {
		File file = new File("artsandpreps.txt");

		try (Scanner scanner = new Scanner(file)) {
			String line = scanner.nextLine();
			String[] answer = line.split(",");

			return answer;

		} catch (FileNotFoundException e) {
			System.err.println(e.getMessage());
		}

		return null;
	}
}
