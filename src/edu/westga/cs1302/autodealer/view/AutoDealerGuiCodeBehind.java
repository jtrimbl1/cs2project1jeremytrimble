package edu.westga.cs1302.autodealer.view;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.model.Dealership;
import edu.westga.cs1302.autodealer.model.DealershipGroup;
import edu.westga.cs1302.autodealer.resources.UI;
import edu.westga.cs1302.autodealer.view.converter.PositiveDecimalNumberStringConverter;
import edu.westga.cs1302.autodealer.view.converter.PositiveWholeNumberStringConverter;
import edu.westga.cs1302.autodealer.viewmodel.AutomobileViewModel;
import edu.westga.cs1302.autodealer.viewmodel.FilterViewModel;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Window;

/**
 * AutoDealerGuiCodeBehind defines the "controller" for AutoDealerGui.fxml.
 * 
 * @author CS1302
 */
public class AutoDealerGuiCodeBehind {
	private DealershipGroup dealers;
	private AutomobileViewModel autoViewModel;
	private FilterViewModel filterViewModel;
	private ObjectProperty<Automobile> selectedAuto;
	private ObjectProperty<Dealership> selectedDealer;

	@FXML
	private AnchorPane pane;

	@FXML
	private MenuItem fileOpenMenuItem;

	@FXML
	private MenuItem fileSaveMenuItem;

	@FXML
	private TextField dealerTextField;

	@FXML
	private TextField makeTextField;

	@FXML
	private TextField modelTextField;

	@FXML
	private TextField yearTextField;

	@FXML
	private TextField milesTextField;

	@FXML
	private TextField priceTextField;

	@FXML
	private Label yearInvalidLabel;

	@FXML
	private Label milesInvalidLabel;

	@FXML
	private Label priceInvalidLabel;

	@FXML
	private Button addButton;

	@FXML
	private Button updateButton;

	@FXML
	private ListView<Automobile> autosListView;

	@FXML
	private Button deleteButton;

	@FXML
	private TextArea summaryTextArea;

	@FXML
	private ComboBox<Dealership> dealersComboBox;

	@FXML
	private TextField makeFilterTextField;

	@FXML
	private TextField minYearFilterTextField;

	@FXML
	private TextField maxYearFilterTextField;

	@FXML
	private Button applyFiltersButton;

	@FXML
	private Button clearFiltersButton;

	/**
	 * Instantiates a new auto dealer gui code behind.
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public AutoDealerGuiCodeBehind() {
		this.dealers = new DealershipGroup();
		this.autoViewModel = new AutomobileViewModel(this.dealers);
		this.filterViewModel = new FilterViewModel(this.dealers);
		this.selectedAuto = new SimpleObjectProperty<Automobile>();
		this.selectedDealer = new SimpleObjectProperty<Dealership>();
	}

	/**
	 * Initializes the GUI components, binding them to the view model properties
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	@FXML
	public void initialize() {
		this.bindComponentsToViewModel();
		this.bindButtonsDisableProperty();
		this.setupListenerForListView();
		this.setupListenerForDealersComboBox();
		this.setupListenersForValidationOfAutoEntry();
		this.setupListenersForValidationOfFilterEntry();

		this.initializeUI();
	}

	private void initializeUI() {
		Integer defaultYear = UI.Text.DEFAULT_YEAR;
		this.yearTextField.setText(defaultYear.toString());
		this.selectedAuto.set(null);

		this.dealersComboBox.getSelectionModel().select(this.dealersComboBox.getItems().get(0));
		this.onClearFilters();
	}

	private void setupListenersForValidationOfAutoEntry() {
		this.yearTextField.textProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue != null) {
				if (!newValue.isEmpty() && !newValue.matches("[1-2]{0,1}|[1-2]\\d{1,3}")) {
					AutoDealerGuiCodeBehind.this.yearTextField.setText(oldValue);
				}
			}
		});

		this.milesTextField.textProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue != null) {
				if (newValue.startsWith(".")) {
					newValue = "0" + newValue;
					AutoDealerGuiCodeBehind.this.milesTextField.setText(newValue);
				}
				if (!newValue.matches("[\\d]*([.]\\d{0,1})?")) {
					AutoDealerGuiCodeBehind.this.milesTextField.setText(oldValue);
				}
			}
		});

		this.priceTextField.textProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue != null) {
				if (newValue.startsWith(".")) {
					newValue = "0" + newValue;
					AutoDealerGuiCodeBehind.this.priceTextField.setText(newValue);
				}
				if (!newValue.matches("[\\d]*([.]\\d{0,2})?")) {
					AutoDealerGuiCodeBehind.this.priceTextField.setText(oldValue);
				}
			}
		});
	}

	private void setupListenersForValidationOfFilterEntry() {
		this.setupYearFilterValidation();
	}

	private void setupYearFilterValidation() {
		this.minYearFilterTextField.textProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue != null) {
				if (!newValue.isEmpty() && !newValue.matches("[1-2]{0,1}|[1-2]\\d{1,3}")) {
					AutoDealerGuiCodeBehind.this.minYearFilterTextField.setText(oldValue);
				}
			}
		});

		this.maxYearFilterTextField.textProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue != null) {
				if (!newValue.isEmpty() && !newValue.matches("[1-2]{0,1}|[1-2]\\d{1,3}")) {
					AutoDealerGuiCodeBehind.this.maxYearFilterTextField.setText(oldValue);
				}
			}
		});
	}

	private void setupListenerForListView() {
		this.autosListView.getSelectionModel().selectedItemProperty().addListener((observable, oldAuto, newAuto) -> {
			if (newAuto != null) {
				this.makeTextField.textProperty().set(newAuto.getMake());
				this.modelTextField.textProperty().set(newAuto.getModel());

				Integer year = newAuto.getYear();
				this.yearTextField.textProperty().set(year.toString());

				Double miles = newAuto.getMiles();
				this.milesTextField.textProperty().set(miles.toString());

				Double price = newAuto.getPrice();

				this.priceTextField.textProperty().set(price.toString());
				this.selectedAuto.set(newAuto);
			}
		});
	}

	private void setupListenerForDealersComboBox() {
		this.dealersComboBox.getSelectionModel().selectedItemProperty()
				.addListener((observable, oldDealer, newDealer) -> {
					if (newDealer != null) {
						this.selectedDealer.set(newDealer);
					}
				});
	}

	private void bindButtonsDisableProperty() {
		BooleanBinding autoSelectedBinding = Bindings
				.isNull(this.autosListView.getSelectionModel().selectedItemProperty());
		this.deleteButton.disableProperty().bind(autoSelectedBinding);
		this.updateButton.disableProperty().bind(autoSelectedBinding);

		this.addButton.disableProperty()
				.bind(this.dealerTextField.textProperty().isEmpty().or(this.makeTextField.textProperty().isEmpty()
						.or(this.modelTextField.textProperty().isEmpty()
								.or(this.yearTextField.textProperty().isEmpty().or(this.milesTextField.textProperty()
										.isEmpty().or(this.priceTextField.textProperty().isEmpty()))))));

	}

	private void bindComponentsToViewModel() {
		this.bindToAutomobileViewModelProperties();
		this.bindToFilterViewModelProperties();
	}

	private void bindToFilterViewModelProperties() {
		this.makeFilterTextField.textProperty().bindBidirectional(this.filterViewModel.makeProperty());
		this.minYearFilterTextField.textProperty().bindBidirectional(this.filterViewModel.minYearProperty(),
				new PositiveWholeNumberStringConverter());
		this.maxYearFilterTextField.textProperty().bindBidirectional(this.filterViewModel.maxYearProperty(),
				new PositiveWholeNumberStringConverter());

		this.dealersComboBox.itemsProperty().bind(this.filterViewModel.dealersProperty());
		this.selectedDealer.bindBidirectional(this.filterViewModel.selectedDealershipProperty());

		this.summaryTextArea.textProperty().bind(this.filterViewModel.summaryProperty());
	}

	private void bindToAutomobileViewModelProperties() {
		this.dealerTextField.textProperty().bindBidirectional(this.autoViewModel.dealerProperty());
		this.makeTextField.textProperty().bindBidirectional(this.autoViewModel.makeProperty());
		this.modelTextField.textProperty().bindBidirectional(this.autoViewModel.modelProperty());
		this.yearTextField.textProperty().bindBidirectional(this.autoViewModel.yearProperty(),
				new PositiveWholeNumberStringConverter());
		this.milesTextField.textProperty().bindBidirectional(this.autoViewModel.milesProperty(),
				new PositiveDecimalNumberStringConverter(1));
		this.priceTextField.textProperty().bindBidirectional(this.autoViewModel.priceProperty(),
				new PositiveDecimalNumberStringConverter(2));

		this.autosListView.itemsProperty().bind(this.autoViewModel.autosProperty());
		this.selectedAuto.bindBidirectional(this.autoViewModel.selectedAutoProperty());
	}

	private void showAlert(String title, String message) {
		Alert alert = new Alert(AlertType.INFORMATION);
		Window owner = this.pane.getScene().getWindow();
		alert.initOwner(owner);
		alert.setTitle(title);
		alert.setContentText(message);
		alert.showAndWait();
	}

	@FXML
	private void onAddAuto() {
		try {
			this.autoViewModel.addDealership();
			this.filterViewModel.updateDealerships();
			if (!this.autoViewModel.addAuto()) {
				this.showAlert("Automobile Not Added",
						"The Automobile was not add. An automobile of the same type already exist.");
			}

			this.filterViewModel.updateSummary();

		} catch (IllegalArgumentException e) {
			this.showAlert("Add auto", e.getMessage());
		} catch (Exception e) {
			this.showAlert("Unknown Error", "An error has occurred, Automobile was not added");
		}
	}

	@FXML
	private void onUpdateAuto() {
		try {
			this.autoViewModel.updateAuto();
			this.filterViewModel.updateDealerships();
			this.filterViewModel.updateSummary();
		} catch (IllegalArgumentException e) {
			this.showAlert("Update auto", e.getMessage());
		}
	}

	@FXML
	private void onDeleteAuto() {
		this.autoViewModel.deleteAuto();
		this.filterViewModel.updateDealerships();
		this.filterViewModel.updateSummary();
	}

	@FXML
	private void onFileOpen() {
		FileChooser fileChooser = new FileChooser();
		this.setFileFilters(fileChooser);

		Window owner = this.pane.getScene().getWindow();
		File selectedFile = fileChooser.showOpenDialog(owner);

		if (selectedFile != null) {
			try {
				this.autoViewModel.loadInventory(selectedFile);
				this.filterViewModel.updateDealerships();
				this.filterViewModel.updateSummary();
			} catch (FileNotFoundException e) {
				this.showAlert(UI.Text.FILE_OPEN, e.getMessage());
			} catch (IOException e) {
				this.showAlert(UI.Text.FILE_OPEN, e.getMessage());
				e.printStackTrace();
			}
		}
	}

	@FXML
	private void onFileSave() {
		FileChooser fileChooser = new FileChooser();
		this.setFileFilters(fileChooser);

		Window owner = this.pane.getScene().getWindow();
		File selectedFile = fileChooser.showSaveDialog(owner);

		if (selectedFile != null) {
			try {
				this.autoViewModel.saveInventory(selectedFile);
			} catch (FileNotFoundException e) {
				this.showAlert(UI.Text.FILE_SAVE, e.getMessage());
			}
		}
	}

	private void setFileFilters(FileChooser fileChooser) {
		ExtensionFilter filter = new ExtensionFilter("Auto Dealer Inventory", "*.adi");
		fileChooser.getExtensionFilters().add(filter);
		filter = new ExtensionFilter("All Files", "*.*");
		fileChooser.getExtensionFilters().add(filter);
	}

	@FXML
	private void onApplyFilters() {
		this.filterViewModel.updateSummary();
	}

	@FXML
	private void onClearFilters() {
		this.makeFilterTextField.setText("");
		this.minYearFilterTextField.setText("");
		this.maxYearFilterTextField.setText("");

		this.dealersComboBox.getSelectionModel().select(this.dealersComboBox.getItems().get(0));
		this.filterViewModel.updateSummary();
	}

}
