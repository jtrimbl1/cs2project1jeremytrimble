package edu.westga.cs1302.autodealer.view.output;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.model.Dealership;
import edu.westga.cs1302.autodealer.model.DealershipGroup;
import edu.westga.cs1302.autodealer.model.Inventory;

/**
 * The Class ReportGenerator.
 * 
 * @author CS1302
 */
public class ReportGenerator {

	private static final String AT = " at ";
	private static final String STRING = ":";
	private static final String S_AT = "s at ";
	private static final String AVERAGE_PRICE = "Average price: ";
	private static final String AUTOS_BY = "Autos by ";
	private static final String FOUND_AT_ANY_OF_THE_DEALERSHIPS = " found at any of the dealerships.";
	private static final String NO = "No ";
	private NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(Locale.US);

	/**
	 * Builds the full summary report of the specified inventory. If inventory is
	 * null, instead of throwing an exception will return a string saying "No
	 * inventory exists.", otherwise builds a summary report of the inventory.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param dealer
	 *            The dealership to build summary report for
	 *
	 * @return A formatted summary string of the dealership's automobile inventory.
	 */
	public String buildFullSummaryReport(Dealership dealer) {
		String summary = "";

		Inventory inventory = dealer.getInventory();

		if (inventory == null) {
			summary = "No inventory exists.";
		} else {
			summary = dealer.getName() + System.lineSeparator();
			summary += "#Automobiles: " + inventory.size() + System.lineSeparator();
		}

		if (inventory.size() > 0) {
			summary += System.lineSeparator();
			summary += this.buildMostAndLeastExpensiveSummaryOutput(inventory);

			summary += System.lineSeparator();
			summary += this.buildAverageMilesReport(inventory);

			summary += System.lineSeparator();
			summary += System.lineSeparator();
			summary += this.buildPriceSegmentBreakdownSummary(3000, inventory);

			summary += System.lineSeparator();
			summary += this.buildPriceSegmentBreakdownSummary(10000, inventory);
		}

		return summary;
	}

	private String buildAverageMilesReport(Inventory inventory) {
		String report = "Average miles: ";
		double averageMiles = inventory.computeAverageMiles();

		DecimalFormat milesFormat = new DecimalFormat("#,###.000");
		report += milesFormat.format(averageMiles);

		return report;
	}

	private String buildMostAndLeastExpensiveSummaryOutput(Inventory inventory) {
		Automobile mostExpensiveAuto = inventory.findMostExpensiveAuto();
		Automobile leastExpensiveAuto = inventory.findLeastExpensiveAuto();

		String report = "Most expensive auto: ";
		report += this.buildIndividualAutomobileReport(mostExpensiveAuto) + System.lineSeparator();

		report += "Least expensive auto: ";
		report += this.buildIndividualAutomobileReport(leastExpensiveAuto) + System.lineSeparator();

		return report;
	}

	private String buildPriceSegmentBreakdownSummary(double priceSegmentRange, Inventory inventory) {
		int[] autoPriceSegmentsCount = inventory.countVehiclesInEachPriceSegment(priceSegmentRange);

		if (autoPriceSegmentsCount == null) {
			return "";
		}

		String priceSegmentSummary = "Vehicles in " + this.currencyFormatter.format(priceSegmentRange) + " segments"
				+ System.lineSeparator();

		double startingPrice = 0;
		double endingPrice = priceSegmentRange;

		for (int i = 0; i < autoPriceSegmentsCount.length; i++) {
			String startingPriceCurrencyFormat = this.currencyFormatter.format(startingPrice);
			String endingPriceCurrencyFormat = this.currencyFormatter.format(endingPrice);
			priceSegmentSummary += startingPriceCurrencyFormat + " - " + endingPriceCurrencyFormat + " : "
					+ this.printStars(autoPriceSegmentsCount[i]) + System.lineSeparator();

			startingPrice = endingPrice + 0.01;
			endingPrice = (priceSegmentRange * (i + 2));
		}

		return priceSegmentSummary;
	}

	private String buildIndividualAutomobileReport(Automobile auto) {
		String output = this.currencyFormatter.format(auto.getPrice()) + " " + auto.getYear() + " " + auto.getMake()
				+ " " + auto.getModel();
		return output;
	}

	private String printStars(int numberOfStars) {
		String stars = "";
		for (int i = 0; i < numberOfStars; i++) {
			stars += "*";
		}
		return stars;
	}

	/**
	 * Builds a report showing all automobiles in inventory at the specified
	 * dealership of the specfied make. Reports that no autos of the make at the
	 * dealership if none exist.
	 * 
	 * @precondition none
	 * @postcondition none
	 * @param make
	 *            The make of autos to display in the report.
	 * @param dealership
	 *            The dealership to look through for the make.
	 * @return A formatted string showing all the automobiles of the specified make
	 *         in the specified dealership
	 */
	public String buildAutosByMakeForDealershipReport(String make, Dealership dealership) {
		if (dealership.getInventory().getAutos().size() == 0) {
			return NO + make + S_AT + dealership;
		}
		String result = "";
		if (dealership.containAutoOfMake(make)) {

			result = AUTOS_BY + make + AT + dealership + STRING + System.lineSeparator();

			for (Automobile current : dealership.getInventory().allAutosByMake(make)) {
				result += current.toString() + System.lineSeparator();
			}
			double averagePrices = dealership.getInventory().averageOfAutoByMake(make);
			result += AVERAGE_PRICE + this.currencyFormatter.format(averagePrices);
		}
		return result;
	}

	/**
	 * Builds a report showing all automobiles in inventory in all dealerships
	 * included in the dealershipgroup. Dealership with no automobiles of the
	 * specified make will not be included in the report, excluding ALL_AUTOS
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param make
	 *            The make of the autos to display in the report
	 * @param dealers
	 *            The dealershipGroup object all the dealerships are a part of
	 * 
	 * @return A formatted string showing all the automobiles of the specified make
	 *         in each of the dealerships in the dealership group.
	 */
	public String buildFullAutoByMakeReport(String make, DealershipGroup dealers) {
		String result = "";
		for (Dealership dealer : dealers.getDealers()) {
			if (!dealer.getName().equals(DealershipGroup.ALL_AUTOS) && dealer.containAutoOfMake(make)) {
				result += this.buildAutosByMakeForDealershipReport(make, dealer) + System.lineSeparator()
						+ System.lineSeparator();
			}
		}

		if (result.isEmpty()) {
			result += NO + make + FOUND_AT_ANY_OF_THE_DEALERSHIPS;
		}
		return result;
	}

	/**
	 * Builds a report showing all automobiles in inventory in all dealerships
	 * included in the dealershipgroup if they are within the specified years.
	 * Dealership with no automobiles of the specified make will not be included in
	 * the report, excluding ALL_AUTOS
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param make
	 *            The make of the autos to display in the report
	 * @param dealers
	 *            The dealershipGroup object all the dealerships are a part of
	 * 
	 * @return A formatted string showing all the automobiles of the specified make
	 *         in each of the dealerships in the dealership group.
	 * 
	 * @param minYear
	 *            The minimum year of automobile to search
	 * @param maxYear
	 *            The maximum year of automobile to search
	 */
	public String buildFullReport(String make, DealershipGroup dealers, int minYear, int maxYear) {
		if (minYear == 0) {
			minYear = Integer.MIN_VALUE;
		}
		if (maxYear == 0) {
			maxYear = Integer.MAX_VALUE;
		}
		String result = "";
		for (Dealership dealer : dealers.getDealers()) {
			if (!dealer.getName().equals(DealershipGroup.ALL_AUTOS) && dealer.containAutoOfMake(make)) {
				result += this.buildAutosByMakeAndYears(make, dealer, minYear, maxYear) + System.lineSeparator()
						+ System.lineSeparator();
			}
		}

		if (result.isEmpty()) {
			result += NO + make + FOUND_AT_ANY_OF_THE_DEALERSHIPS;
		}
		return result;
	}

	private String buildAutosByMakeAndYears(String make, Dealership dealership, int minYear, int maxYear) {
		String result = "";
		if (dealership.getInventory().getAutos().size() == 0) {
			return NO + make + S_AT + dealership;
		}

		result = AUTOS_BY + make + AT + dealership + STRING + System.lineSeparator();

		for (Automobile current : dealership.getInventory().allAutosByMake(make)) {
			if (current.getYear() >= minYear && current.getYear() <= maxYear) {
				result += current.toString() + System.lineSeparator();
			}

		}
		double averagePrices = dealership.getInventory().averageOfAutoByMake(make);
		result += AVERAGE_PRICE + this.currencyFormatter.format(averagePrices);
		return result;
	}
}
