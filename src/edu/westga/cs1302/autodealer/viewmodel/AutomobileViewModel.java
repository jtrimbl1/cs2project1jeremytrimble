package edu.westga.cs1302.autodealer.viewmodel;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import edu.westga.cs1302.autodealer.datatier.AutoDealerFileReader;
import edu.westga.cs1302.autodealer.datatier.AutoDealerFileWriter;
import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.model.Dealership;
import edu.westga.cs1302.autodealer.model.DealershipGroup;
import edu.westga.cs1302.autodealer.resources.UI;
import edu.westga.cs1302.autodealer.view.converter.StringConverter;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;

/**
 * The Class AutoDealerViewModel.
 * 
 * @author CS1302
 */
public class AutomobileViewModel {
	private DealershipGroup dealers;
	private Dealership allAutos;

	private StringProperty dealerProperty;
	private StringProperty makeProperty;
	private StringProperty modelProperty;
	private IntegerProperty yearProperty;
	private DoubleProperty milesProperty;
	private DoubleProperty priceProperty;
	private ObjectProperty<Automobile> selectedAutoProperty;

	private ListProperty<Automobile> autosProperty;

	/**
	 * Instantiates a new auto dealer view model.
	 *
	 * @precondition dealers != null
	 * @postcondition none
	 * 
	 * @param dealers
	 *            the dealers
	 */
	public AutomobileViewModel(DealershipGroup dealers) {
		if (dealers == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.DEALERS_CANNOT_BE_NULL);
		}
		this.dealers = dealers;
		this.allAutos = this.dealers.findDealership(DealershipGroup.ALL_AUTOS);

		this.dealerProperty = new SimpleStringProperty();
		this.makeProperty = new SimpleStringProperty();
		this.modelProperty = new SimpleStringProperty();
		this.yearProperty = new SimpleIntegerProperty();
		this.milesProperty = new SimpleDoubleProperty();
		this.priceProperty = new SimpleDoubleProperty();

		this.autosProperty = new SimpleListProperty<Automobile>(
				FXCollections.observableArrayList(this.allAutos.getInventory().getAutos()));
		this.selectedAutoProperty = new SimpleObjectProperty<Automobile>();
	}

	/**
	 * Dealer property.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the make property
	 */
	public StringProperty dealerProperty() {
		return this.dealerProperty;
	}

	/**
	 * Make property.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the make property
	 */
	public StringProperty makeProperty() {
		return this.makeProperty;
	}

	/**
	 * Model property.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the model property
	 */
	public StringProperty modelProperty() {
		return this.modelProperty;
	}

	/**
	 * Year property.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the year property
	 */
	public IntegerProperty yearProperty() {
		return this.yearProperty;
	}

	/**
	 * Miles property.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the miles property
	 */
	public DoubleProperty milesProperty() {
		return this.milesProperty;
	}

	/**
	 * Price property.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the price property
	 */
	public DoubleProperty priceProperty() {
		return this.priceProperty;
	}

	/**
	 * Selected auto property.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the selected auto property
	 */
	public ObjectProperty<Automobile> selectedAutoProperty() {
		return this.selectedAutoProperty;
	}

	/**
	 * Autos property.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the list of autos property
	 */
	public ListProperty<Automobile> autosProperty() {
		return this.autosProperty;
	}

	/**
	 * Adds dealership to the dealership group
	 * 
	 * @return true if new dealership and added, false if existing dealership and
	 *         not added
	 */
	public boolean addDealership() {
		var dealerName = this.dealerProperty.get();
		return this.dealers.addDealership(dealerName);
	}

	/**
	 * Adds an automobile with specified attributes to the inventory.
	 *
	 * @return true if auto added successfully, false otherwise
	 */
	public boolean addAuto() {
		String make = this.makeProperty.get();
		String model = this.modelProperty.get();
		int year = this.yearProperty.get();
		double miles = this.milesProperty.get();
		double price = this.priceProperty.get();
		String dealerName = this.dealerProperty.get();
		dealerName = StringConverter.formatString(dealerName);

		var auto = new Automobile(make, model, year, miles, price);

		if (this.dealers.addAuto(DealershipGroup.ALL_AUTOS, auto) && this.dealers.addAuto(dealerName, auto)) {
			this.resetAutomobileEntryProperties();
			this.autosProperty.set(FXCollections.observableArrayList(this.allAutos.getInventory().getAutos()));
			return true;
		}

		return false;
	}

	private void resetAutomobileEntryProperties() {
		this.dealerProperty.set("");
		this.makeProperty.set("");
		this.modelProperty.set("");
		this.yearProperty.set(UI.Text.DEFAULT_YEAR);
		this.milesProperty.set(0.0);
		this.priceProperty.set(0.0);
	}

	/**
	 * Update selected auto with new miles and price
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return true, if successful
	 */
	public boolean updateAuto() {
		Automobile auto = this.selectedAutoProperty.get();
		if (auto != null) {
			double miles = this.milesProperty.get();
			double price = this.priceProperty.get();

			auto.setMiles(miles);
			auto.setPrice(price);

			this.resetAutomobileEntryProperties();
			this.autosProperty.set(FXCollections.observableArrayList(this.allAutos.getInventory().getAutos()));
			return true;
		}

		return false;
	}

	/**
	 * Delete selected auto from the inventory.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return true, if auto successfully deleted, false otherwise
	 */
	public boolean deleteAuto() {
		Automobile auto = this.selectedAutoProperty.get();
		Dealership nonAllAutosDealer = this.dealers.findDealershipContainingAuto(auto);

		if (nonAllAutosDealer != null) {
			nonAllAutosDealer.getInventory().remove(auto);
		}

		if (this.allAutos.getInventory().remove(auto)) {
			this.resetAutomobileEntryProperties();
			this.autosProperty.set(FXCollections.observableArrayList(this.allAutos.getInventory().getAutos()));
			return true;
		}

		return false;
	}

	/**
	 * Save the autodealer inventory to the specified File.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @param file
	 *            the file to save the autodealer inventory to
	 * @throws FileNotFoundException
	 *             When file not found
	 */
	public void saveInventory(File file) throws FileNotFoundException {
		AutoDealerFileWriter writer = new AutoDealerFileWriter(file);
		writer.write(this.dealers);
	}

	/**
	 * Loads the autodealer inventory file using the specified File object.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param file
	 *            The file to load the auto dealer inventory from
	 * @throws IOException
	 *             throw IOException
	 */
	public void loadInventory(File file) throws IOException {
		AutoDealerFileReader reader = new AutoDealerFileReader(file);
		reader.loadAutosIntoDealershipGroup(this.dealers);

		this.resetAutomobileEntryProperties();
		this.autosProperty.set(FXCollections.observableArrayList(this.allAutos.getInventory().getAutos()));
	}
}
