/*
 * 
 */
package edu.westga.cs1302.autodealer.viewmodel;

import edu.westga.cs1302.autodealer.model.Dealership;
import edu.westga.cs1302.autodealer.model.DealershipGroup;
import edu.westga.cs1302.autodealer.resources.UI;
import edu.westga.cs1302.autodealer.view.output.ReportGenerator;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;

/**
 * The Class FilterViewModel.
 *
 * @author CS1302
 */
public class FilterViewModel {
	private ReportGenerator report;
	private DealershipGroup dealers;

	private ListProperty<Dealership> dealersProperty;
	private ObjectProperty<Dealership> selectedDealershipProperty;

	private StringProperty makeProperty;
	private IntegerProperty minYearProperty;
	private IntegerProperty maxYearProperty;

	private StringProperty summaryProperty;

	/**
	 * Instantiates a new filter view model.
	 *
	 * @precondition dealers != null
	 * @postcondition none
	 * 
	 * @param dealers
	 *            the dealers
	 */
	public FilterViewModel(DealershipGroup dealers) {
		if (dealers == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.DEALERS_CANNOT_BE_NULL);
		}

		this.dealers = dealers;
		this.report = new ReportGenerator();

		this.makeProperty = new SimpleStringProperty();
		this.minYearProperty = new SimpleIntegerProperty();
		this.maxYearProperty = new SimpleIntegerProperty();

		this.selectedDealershipProperty = new SimpleObjectProperty<Dealership>();
		this.dealersProperty = new SimpleListProperty<Dealership>(
				FXCollections.observableArrayList(this.dealers.getDealers()));

		this.summaryProperty = new SimpleStringProperty();
	}

	/**
	 * Update dealerships.
	 */
	public void updateDealerships() {
		this.dealersProperty.set(FXCollections.observableArrayList(this.dealers.getDealers()));
	}

	/**
	 * Update summary report.
	 *
	 * @precondition none
	 * @postcondition none
	 */
	public void updateSummary() {
		String make = this.makeProperty.get();
		Dealership selectedDealer = this.selectedDealershipProperty.get();
		int minYear = this.minYearProperty.get();
		int maxYear = this.maxYearProperty.get();

		if (!make.isEmpty()) {
			if (selectedDealer.getName().equals(DealershipGroup.ALL_AUTOS)) {
				String autosByMakeForAllDealers = this.report.buildFullReport(make, this.dealers, minYear, maxYear);
				this.summaryProperty.set(autosByMakeForAllDealers);
			} else {
				String autosByMake = this.report.buildFullReport(make, this.dealers, minYear, maxYear);
				this.summaryProperty.set(autosByMake);
			}
		} else {
			String summaryReport = this.report.buildFullSummaryReport(this.selectedDealershipProperty.get());
			this.summaryProperty.set(summaryReport);
		}
	}

	/**
	 * Gets the dealers property.
	 *
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the dealersProperty
	 */
	public ListProperty<Dealership> dealersProperty() {
		return this.dealersProperty;
	}

	/**
	 * Gets the selected dealership property.
	 *
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the selectedDealershipProperty
	 */
	public ObjectProperty<Dealership> selectedDealershipProperty() {
		return this.selectedDealershipProperty;
	}

	/**
	 * Gets the make property.
	 *
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the makeProperty
	 */
	public StringProperty makeProperty() {
		return this.makeProperty;
	}

	/**
	 * Gets the min year property.
	 *
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the minYearProperty
	 */
	public IntegerProperty minYearProperty() {
		return this.minYearProperty;
	}

	/**
	 * Gets the max year property.
	 *
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the maxYearProperty
	 */
	public IntegerProperty maxYearProperty() {
		return this.maxYearProperty;
	}

	/**
	 * Summary property.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the summary property
	 */
	public StringProperty summaryProperty() {
		return this.summaryProperty;
	}

}
