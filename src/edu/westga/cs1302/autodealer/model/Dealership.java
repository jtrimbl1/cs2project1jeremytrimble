package edu.westga.cs1302.autodealer.model;

import edu.westga.cs1302.autodealer.view.converter.StringConverter;

/**
 * The Class Dealership.
 * 
 * @author CS1302
 */
public class Dealership {
	private static final String DEALERSHIP_NAME_CANNOT_BE_EMPTY = "name cannot be empty.";
	private static final String DEALERSHIP_NAME_CANNOT_BE_NULL = "name cannot be null.";

	private String name;
	private Inventory inventory;

	/**
	 * Instantiates a new dealership.
	 * 
	 * @precondition none
	 * @postcondition getName() == "Unknown"; getInventory().size() == 0
	 */
	public Dealership() {
		this.name = "Unknown";
		this.inventory = new Inventory();
	}

	/**
	 * Instantiates a new dealership.
	 * 
	 * @precondition name cannot be null or empty
	 * @postcondition getName() == name; getInventory().size() == 0
	 *
	 * @param name
	 *            the name
	 */
	public Dealership(String name) {
		if (name == null) {
			throw new IllegalArgumentException(DEALERSHIP_NAME_CANNOT_BE_NULL);
		}

		if (name.isEmpty()) {
			throw new IllegalArgumentException(DEALERSHIP_NAME_CANNOT_BE_EMPTY);
		}

		this.name = StringConverter.formatString(name);
		this.inventory = new Inventory();
	}

	/**
	 * Gets the name.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Gets the inventory.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the inventory
	 */
	public Inventory getInventory() {
		return this.inventory;
	}

	@Override
	public String toString() {
		return this.name + "; #Autos: " + this.inventory.size();
	}

	/**
	 * Returns whether or not an automobile in apart of the dealership
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param auto
	 *            The automobile being searched for
	 * 
	 * @return true if the specified automobile is apart of the dealership else
	 *         false
	 */
	public boolean contains(Automobile auto) {
		for (Automobile currAuto : this.inventory.getAutos()) {
			if (currAuto == auto) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Returns whether a automobile of the specified make is in the dealership
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param make
	 *            The make of an automobile to search for
	 * @return true if an automobile of specified make exist
	 */
	public boolean containAutoOfMake(String make) {
		for (Automobile current : this.inventory.getAutos()) {
			if (current.getMake().equalsIgnoreCase(make)) {
				return true;
			}
		}

		return false;
	}

}
