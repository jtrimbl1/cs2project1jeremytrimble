package edu.westga.cs1302.autodealer.model;

import java.util.ArrayList;

import edu.westga.cs1302.autodealer.resources.UI;

/**
 * The Class DealershipGroup - store a collection of dealerships
 * 
 * @author CS1302
 */
public class DealershipGroup {
	public static final String ALL_AUTOS = "ALL AUTOS";

	private ArrayList<Dealership> dealers;

	/**
	 * Instantiates a new dealership group.
	 */
	public DealershipGroup() {
		this.dealers = new ArrayList<Dealership>();
		this.addDealership(ALL_AUTOS);
	}

	/**
	 * Adds the dealership.
	 * 
	 * @precondition name not null; name not empty
	 * @postcondition if new dealership then adds new dealership
	 *
	 * @param name
	 *            the name
	 * @return true, if adding new dealership, false if dealership already exists
	 */
	public boolean addDealership(String name) {
		if (name == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NAME_CANNOT_BE_NULL);
		}

		if (name.isEmpty()) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NAME_CANNOT_BE_EMPTY);
		}

		for (Dealership currDealer : this.dealers) {
			if (name.equalsIgnoreCase(currDealer.getName())) {
				return false;
			}
		}

		Dealership dealer = new Dealership(name);
		return this.dealers.add(dealer);
	}

	/**
	 * Finds dealership with specified name
	 *
	 * @param name
	 *            the name
	 * @return the dealership if found, null otherwise
	 */
	public Dealership findDealership(String name) {
		if (name == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NAME_CANNOT_BE_NULL);
		}

		for (Dealership currDealer : this.dealers) {
			if (name.equalsIgnoreCase(currDealer.getName())) {
				return currDealer;
			}
		}

		return null;
	}

	/**
	 * Determines if the dealership group contains the given dealership
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param name
	 *            The name of the dealership to search for
	 * 
	 * @return true if the dealership exist, else false
	 */
	public boolean contains(String name) {
		return (this.findDealership(name) != null);
	}

	/**
	 * Adds the specified auto the the specified dealer
	 * 
	 * @precondition dealerName not null; auto not null
	 * @postconditon auto added to specified dealership
	 *
	 * @param dealerName
	 *            the dealer name
	 * @param auto
	 *            the auto
	 * @return true, if auto successfully added; false, otherwise
	 */
	public boolean addAuto(String dealerName, Automobile auto) {
		if (dealerName == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.DEALERNAME_CANNOT_BE_NULL);
		}

		if (auto == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.AUTO_CANNOT_BE_NULL);
		}

		Dealership dealer = this.findDealership(dealerName);
		if (dealer != null) {
			return dealer.getInventory().add(auto);
		}

		return false;
	}

	/**
	 * Removes the specified auto from the specified dealer name.
	 * 
	 * @precondition dealerName != null; auto != null
	 * @postcondition if auto found at dealer, the inventory size is for that dealer
	 *                is reduced by one
	 *
	 * @param dealerName
	 *            the dealer name
	 * @param auto
	 *            the auto
	 * @return true, if successful
	 */
	public boolean removeAuto(String dealerName, Automobile auto) {
		if (dealerName == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.DEALERNAME_CANNOT_BE_NULL);
		}

		if (auto == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.AUTO_CANNOT_BE_NULL);
		}

		Dealership dealer = this.findDealership(dealerName);
		if (dealer != null) {
			return dealer.getInventory().remove(auto);
		}

		return false;
	}

	/**
	 * Gets the dealers.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the dealers
	 */
	public ArrayList<Dealership> getDealers() {
		return this.dealers;
	}

	/**
	 * Creates a dealership if one of the specified name doesn't already exist, else
	 * it returns the dealership that already exist
	 * 
	 * @precondition name != null && !name.isEmpty()
	 * @postcondition none
	 * 
	 * @param name
	 *            name of the dealership that is to be return after being found or
	 *            created
	 * @return a dealership with the specified name
	 */
	public Dealership createOrGetDealership(String name) {
		if (name == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NAME_CANNOT_BE_NULL);
		}
		if (name.isEmpty()) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NAME_CANNOT_BE_EMPTY);
		}

		if (this.contains(name)) {
			return this.findDealership(name);
		} else {
			Dealership dealer = new Dealership(name);
			this.dealers.add(dealer);
			return dealer;
		}

	}

	/**
	 * Returns the dealership that contains the specified automobile
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param auto
	 *            the automobile that is being search for in the dealerships
	 * @return the dealership that contains the specified automobile. null if no
	 *         dealership contains the automobile
	 */
	public Dealership findDealershipContainingAuto(Automobile auto) {
		for (Dealership dealer : this.dealers) {
			if (!dealer.getName().equals(DealershipGroup.ALL_AUTOS)) {
				if (dealer.contains(auto)) {
					return dealer;
				}
			}
		}
		return null;
	}

}
