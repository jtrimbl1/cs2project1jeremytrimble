package edu.westga.cs1302.autodealer.datatier;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.model.Dealership;
import edu.westga.cs1302.autodealer.model.DealershipGroup;
import edu.westga.cs1302.autodealer.resources.UI;

/**
 * The Class AutoDealerFileReader. Reads an .adi (Auto Dealer Inventory) file
 * which is a CSV file with the following format: make,model,year,miles,price
 * 
 * @author CS1302
 */
public class AutoDealerFileReader {
	public static final String FIELD_SEPARATOR = ",";

	private File inventoryFile;
	private File logFile;

	/**
	 * Instantiates a new auto dealer file reader.
	 *
	 * @precondition inventoryFile != null
	 * @postcondition none
	 * 
	 * @param inventoryFile
	 *            the inventory file
	 */
	public AutoDealerFileReader(File inventoryFile) {
		if (inventoryFile == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.INVENTORY_FILE_CANNOT_BE_NULL);
		}

		this.inventoryFile = inventoryFile;
		this.logFile = new File("readErrors.log");
	}

	/**
	 * Opens the associated adi file and reads all the autos in the file one line at
	 * a time. Parses each line and creates an automobile object and stores it in an
	 * ArrayList of Automobile objects. Once the file has been completely read the
	 * ArrayList of Automobiles is returned from the method. Assumes all autos in
	 * the file are for the same dealership.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return Collection of Automobile objects read from the file.
	 * @throws IOException
	 *             Throws an IOException
	 */
	public ArrayList<Automobile> loadAllAutos() throws IOException {
		ArrayList<Automobile> autos = new ArrayList<Automobile>();

		try (Scanner input = new Scanner(this.inventoryFile)) {
			while (input.hasNextLine()) {
				String line = input.nextLine();
				String[] fields = this.splitLine(line, AutoDealerFileReader.FIELD_SEPARATOR);

				Automobile auto = this.makeAutomobile(fields);
				autos.add(auto);
			}
		} catch (Exception e) {
			if (!this.logFile.exists()) {
				this.logFile.createNewFile();
			}

			FileWriter writer = new FileWriter(this.logFile, true);
			writer.write(e.getMessage() + System.lineSeparator());
			writer.close();
		}

		return autos;
	}

	private Automobile makeAutomobile(String[] fields) {
		String make = fields[0];
		String model = fields[1];
		int year = Integer.parseInt(fields[2]);
		double miles = Double.parseDouble(fields[3]);
		double price = Double.parseDouble(fields[4]);
		return new Automobile(make, model, year, miles, price);
	}

	private String[] splitLine(String line, String fieldSeparator) {
		return line.split(fieldSeparator);
	}

	/**
	 * Loads all the automobiles in into a dealership group with the name specified.
	 *
	 * @precondition none
	 * @postcondition all autos in the file all loaded into a dealership group with
	 *                the name specified.
	 * 
	 * @param dealershipGroup
	 *            The dealershipGroup that the automobiles are to be added to
	 * @throws IOException
	 *             throws IOException
	 * 
	 */
	public void loadAutosIntoDealershipGroup(DealershipGroup dealershipGroup) throws IOException {

		try (Scanner input = new Scanner(this.inventoryFile)) {
			while (input.hasNextLine()) {
				String line = input.nextLine();
				String[] fields = this.splitLine(line, AutoDealerFileReader.FIELD_SEPARATOR);

				Dealership dealer = dealershipGroup.createOrGetDealership(fields[0]);

				String[] autoFields = Arrays.copyOfRange(fields, 1, 6);
				Automobile auto = this.makeAutomobile(autoFields);
				dealer.getInventory().add(auto);
				dealershipGroup.addAuto(DealershipGroup.ALL_AUTOS, auto);
			}
		} catch (Exception e) {
			if (!this.logFile.exists()) {
				this.logFile.createNewFile();
			}

			FileWriter writer = new FileWriter(this.logFile, true);
			writer.write(e.getMessage() + System.lineSeparator());
			writer.close();
		}
	}

}
