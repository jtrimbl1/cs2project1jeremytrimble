package edu.westga.cs1302.autodealer.datatier;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;

import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.model.Dealership;
import edu.westga.cs1302.autodealer.model.DealershipGroup;
import edu.westga.cs1302.autodealer.resources.UI;

/**
 * The Class AlbumFileWriter.
 * 
 * @author CS1302
 */
public class AutoDealerFileWriter {

	private File inventoryFile;

	/**
	 * Instantiates a new auto file writer.
	 *
	 * @precondition inventoryFile != null
	 * @postcondition none
	 * 
	 * @param inventoryFile
	 *            the inventory file
	 */
	public AutoDealerFileWriter(File inventoryFile) {
		if (inventoryFile == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.INVENTORY_FILE_CANNOT_BE_NULL);
		}

		this.inventoryFile = inventoryFile;

	}

	/**
	 * Writes all the autos in the dealership to the specified auto file. Each auto
	 * will be on a separate line and of the following format:
	 * make,model,year,miles,price
	 * 
	 * @precondition autos != null
	 * @postcondition none
	 * 
	 * @param autos
	 *            The collection of autos to write to file.
	 * @throws FileNotFoundException
	 *             throws file not found exception
	 */
	public void write(ArrayList<Automobile> autos) throws FileNotFoundException {
		if (autos == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.AUTOS_CANNOT_BE_NULL);
		}

		try (PrintWriter writer = new PrintWriter(this.inventoryFile)) {
			for (Automobile currAuto : autos) {
				String output = currAuto.getMake() + AutoDealerFileReader.FIELD_SEPARATOR;
				output += currAuto.getModel() + AutoDealerFileReader.FIELD_SEPARATOR;
				output += currAuto.getYear() + AutoDealerFileReader.FIELD_SEPARATOR;
				output += currAuto.getMiles() + AutoDealerFileReader.FIELD_SEPARATOR;
				output += currAuto.getPrice();

				writer.println(output);
			}
		}

	}

	/**
	 * Writes all the autos in a dealership group to file. Each auto will be on a
	 * separate line and of the following format:
	 * dealership,make,model,year,miles,price
	 * 
	 * @precondition dealers != null
	 * @postcondition none
	 * 
	 * @param dealers
	 *            The DealershipGroup that contains all the dealerships and auto
	 *            that are to populate the file
	 * @throws FileNotFoundException
	 *             throws file not found exception
	 */
	public void write(DealershipGroup dealers) throws FileNotFoundException {
		if (dealers == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.DEALERS_CANNOT_BE_NULL);
		}

		try (PrintWriter writer = new PrintWriter(this.inventoryFile)) {
			for (Dealership dealer : dealers.getDealers()) {
				if (!dealer.getName().equals(DealershipGroup.ALL_AUTOS)) {
					for (Automobile currAuto : dealer.getInventory().getAutos()) {
						String output = dealer.getName() + AutoDealerFileReader.FIELD_SEPARATOR;
						output += currAuto.getMake() + AutoDealerFileReader.FIELD_SEPARATOR;
						output += currAuto.getModel() + AutoDealerFileReader.FIELD_SEPARATOR;
						output += currAuto.getYear() + AutoDealerFileReader.FIELD_SEPARATOR;
						output += currAuto.getMiles() + AutoDealerFileReader.FIELD_SEPARATOR;
						output += currAuto.getPrice();
						writer.println(output);
					}
				}
			}
		}
	}

}
