package edu.westga.cs1302.autodealer.test.inventory;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.model.Inventory;

class TestAverageOfAutosByMake {

	@Test
	void testEmptyInventory() {
		Inventory inventory = new Inventory();

		assertEquals(0, inventory.averageOfAutoByMake("Ford"));
	}

	@Test
	void testSingleInventorySingleAverage() {
		Inventory inventory = new Inventory();
		inventory.add(new Automobile("Ford", "Tarus", 2001, 165876, 2100));

		assertEquals(2100, inventory.averageOfAutoByMake("Ford"));
	}

	@Test
	void testMultipleInventorySingleAverage() {
		Inventory inventory = new Inventory();
		inventory.add(new Automobile("Ford", "Tarus", 2001, 165876, 2100));
		inventory.add(new Automobile("Chevy", "Silverado", 2010, 165876, 15000));

		assertEquals(2100, inventory.averageOfAutoByMake("Ford"));
	}

	@Test
	void testMultipleInventoryMultipleAverage() {
		Inventory inventory = new Inventory();
		inventory.add(new Automobile("Ford", "Tarus", 2001, 165876, 2000));
		inventory.add(new Automobile("Chevy", "Silverado", 2010, 165876, 15000));
		inventory.add(new Automobile("Ford", "Van", 2015, 20000, 20000));

		assertEquals(11000, inventory.averageOfAutoByMake("Ford"));
	}

}
