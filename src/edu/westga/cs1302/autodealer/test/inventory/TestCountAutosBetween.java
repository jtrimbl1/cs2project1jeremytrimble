package edu.westga.cs1302.autodealer.test.inventory;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.model.Inventory;

class TestCountAutosBetween {

	@Test
	void testStartYearGreaterThanEndYear() {
		Inventory inventory = new Inventory();
		assertThrows(IllegalArgumentException.class, () -> inventory.countAutosBetween(2001, 2000));

	}

	@Test
	void testEmptyInventory() {
		Inventory inventory = new Inventory();
		int count = inventory.countAutosBetween(2000, 2001);
		assertEquals(0, count);
	}

	@Test
	void testOneCarInventoryNotWithinRange() {
		Inventory inventory = new Inventory();
		inventory.add(new Automobile("Ford", "Focus", 2009, 108132.2, 6900));
		int count = inventory.countAutosBetween(2000, 2001);
		assertEquals(0, count);
	}

	@Test
	void testOneCarInventoryWithinRange() {
		Inventory inventory = new Inventory();
		inventory.add(new Automobile("Ford", "Focus", 2000, 108132.2, 6900));
		int count = inventory.countAutosBetween(2000, 2001);
		assertEquals(1, count);
	}

	@Test
	void testOneCarInventoryWithYearBelowLowerBoundYear() {
		Inventory inventory = new Inventory();
		inventory.add(new Automobile("Ford", "Focus", 1999, 108132.2, 6900));
		int count = inventory.countAutosBetween(2000, 2002);
		assertEquals(0, count);
	}

	@Test
	void testOneCarInventoryWithYearAtLowerBoundYear() {
		Inventory inventory = new Inventory();
		inventory.add(new Automobile("Ford", "Focus", 2000, 108132.2, 6900));
		int count = inventory.countAutosBetween(2000, 2002);
		assertEquals(1, count);
	}

	@Test
	void testOneCarInventoryWithYearAboveLowerBoundYear() {
		Inventory inventory = new Inventory();
		inventory.add(new Automobile("Ford", "Focus", 2001, 108132.2, 6900));
		int count = inventory.countAutosBetween(2000, 2002);
		assertEquals(1, count);
	}

	@Test
	void testOneCarInventoryWithYearBelowUpperBoundYear() {
		Inventory inventory = new Inventory();
		inventory.add(new Automobile("Ford", "Focus", 2001, 108132.2, 6900));
		int count = inventory.countAutosBetween(2000, 2002);
		assertEquals(1, count);
	}

	@Test
	void testOneCarInventoryWithYearAtUppderBoundYear() {
		Inventory inventory = new Inventory();
		inventory.add(new Automobile("Ford", "Focus", 2002, 108132.2, 6900));
		int count = inventory.countAutosBetween(2000, 2002);
		assertEquals(1, count);
	}

	@Test
	void testOneCarInventoryWithYearAboveUpperBoundYear() {
		Inventory inventory = new Inventory();
		inventory.add(new Automobile("Ford", "Focus", 2003, 108132.2, 6900));
		int count = inventory.countAutosBetween(2000, 2002);
		assertEquals(0, count);
	}

	@Test
	void testMultiCarsInInventoryNoneWithinRange() {
		Inventory inventory = new Inventory();
		inventory.add(new Automobile("Ford", "Focus", 2008, 108132.2, 6900));
		inventory.add(new Automobile("Ford", "Focus", 2009, 108132.2, 6900));
		inventory.add(new Automobile("Ford", "Focus", 2010, 108132.2, 6900));
		int count = inventory.countAutosBetween(2000, 2001);
		assertEquals(0, count);
	}

	@Test
	void testMultiCarsInInventorySomeWithinRange() {
		Inventory inventory = new Inventory();
		inventory.add(new Automobile("Ford", "Focus", 1999, 108132.2, 6900));
		inventory.add(new Automobile("Ford", "Focus", 2000, 108132.2, 6900));
		inventory.add(new Automobile("Ford", "Focus", 2001, 108132.2, 6900));
		inventory.add(new Automobile("Ford", "Focus", 2002, 108132.2, 6900));
		int count = inventory.countAutosBetween(2000, 2001);
		assertEquals(2, count);
	}

	@Test
	void testMultiCarsInInventoryAllWithinRangeTest() {
		Inventory inventory = new Inventory();
		inventory.add(new Automobile("Ford", "Focus", 1999, 108132.2, 6900));
		inventory.add(new Automobile("Ford", "Focus", 2000, 108132.2, 6900));
		inventory.add(new Automobile("Ford", "Focus", 2001, 108132.2, 6900));
		inventory.add(new Automobile("Ford", "Focus", 2002, 108132.2, 6900));
		int count = inventory.countAutosBetween(1999, 2002);
		assertEquals(4, count);
	}

}
