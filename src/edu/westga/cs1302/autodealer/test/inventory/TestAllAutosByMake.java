package edu.westga.cs1302.autodealer.test.inventory;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.model.Inventory;

class TestAllAutosByMake {

	@Test
	void testEmptyListCreatedFromEmptyInventory() {
		Inventory inventory = new Inventory();

		assertEquals(0, inventory.allAutosByMake("Ford").size());
	}

	@Test
	void testEmptyListFromNonEmptyInventory() {
		Inventory inventory = new Inventory();
		inventory.add(new Automobile("Ford", "Fusion", 2009, 68501.32, 12368.00));
		inventory.add(new Automobile("Ford", "Focus", 2010, 68501.32, 12368.00));
		inventory.add(new Automobile("Ford", "Tarus", 2009, 68501.32, 12368.00));

		assertEquals(0, inventory.allAutosByMake("Chevy").size());
	}

	@Test
	void testSingleInventory() {
		Inventory inventory = new Inventory();
		inventory.add(new Automobile("Ford", "Fusion", 2009, 68501.32, 12368.00));
		inventory.add(new Automobile("Chevy", "Silverado", 2010, 68501.32, 12368.00));
		inventory.add(new Automobile("Ford", "Tarus", 2009, 68501.32, 12368.00));

		assertEquals(1, inventory.allAutosByMake("Chevy").size());
	}

	@Test
	void testMultipleInventory() {
		Inventory inventory = new Inventory();
		inventory.add(new Automobile("Ford", "Fusion", 2009, 68501.32, 12368.00));
		inventory.add(new Automobile("Chevy", "Silverado", 2010, 68501.32, 12368.00));
		inventory.add(new Automobile("Chevy", "Traverse", 2009, 68501.32, 12368.00));

		assertEquals(2, inventory.allAutosByMake("Chevy").size());
	}

}
