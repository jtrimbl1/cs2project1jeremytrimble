package edu.westga.cs1302.autodealer.test.dealership;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.model.Dealership;

class TestContains {

	@Test
	void testEmptyInventory() {
		Dealership dealer = new Dealership("Harry's Autos");
		Automobile auto1 = new Automobile("Ford", "Tarus", 2010, 52635, 2500);

		assertEquals(false, dealer.contains(auto1));
	}

	@Test
	void testSingleInventoryDoesContain() {
		Dealership dealer = new Dealership("Harry's Autos");
		Automobile auto1 = new Automobile("Ford", "Tarus", 2010, 52635, 2500);

		dealer.getInventory().add(auto1);
		assertEquals(true, dealer.contains(auto1));
	}

	@Test
	void testMultipleInventoryDoesContain() {
		Dealership dealer = new Dealership("Harry's Autos");
		Automobile auto1 = new Automobile("Ford", "Tarus", 2010, 52635, 2500);
		Automobile auto2 = new Automobile("Ford", "Fusion", 2000, 65986, 7500);

		dealer.getInventory().add(auto1);
		dealer.getInventory().add(auto2);

		assertEquals(true, dealer.contains(auto1));
	}

	@Test
	void testMultipleInventoryDoesntContain() {
		Dealership dealer = new Dealership("Harry's Autos");
		Automobile auto1 = new Automobile("Ford", "Tarus", 2010, 52635, 2500);
		Automobile auto2 = new Automobile("Ford", "Fusion", 2000, 65986, 7500);
		Automobile auto3 = new Automobile("Ford", "Fusion", 2000, 65986, 7500);

		dealer.getInventory().add(auto1);
		dealer.getInventory().add(auto2);

		assertEquals(false, dealer.contains(auto3));
	}

}
