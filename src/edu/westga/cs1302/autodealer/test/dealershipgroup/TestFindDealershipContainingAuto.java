package edu.westga.cs1302.autodealer.test.dealershipgroup;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.model.Dealership;
import edu.westga.cs1302.autodealer.model.DealershipGroup;

class TestFindDealershipContainingAuto {

	@Test
	void testNullAuto() {
		DealershipGroup group = new DealershipGroup();

		assertEquals(null, group.findDealershipContainingAuto(null));
	}

	@Test
	void testAutoNotInDealership() {
		DealershipGroup group = new DealershipGroup();
		group.addDealership("Buy Here");
		Automobile auto1 = new Automobile("Toyota", "Prius", 2010, 134789, 7856);

		assertEquals(null, group.findDealershipContainingAuto(auto1));
	}

	@Test
	void testAutoIsInDealership() {
		DealershipGroup group = new DealershipGroup();
		group.addDealership("Buy Here");
		Automobile auto1 = new Automobile("Toyota", "Prius", 2010, 134789, 7856);
		group.addAuto("Buy Here", auto1);
		Dealership actual = group.findDealership("Buy Here");
		Dealership expected = group.findDealershipContainingAuto(auto1);

		assertEquals(actual, expected);
	}

}
