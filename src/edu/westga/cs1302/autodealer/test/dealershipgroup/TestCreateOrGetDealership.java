package edu.westga.cs1302.autodealer.test.dealershipgroup;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.autodealer.model.Dealership;
import edu.westga.cs1302.autodealer.model.DealershipGroup;

class TestCreateOrGetDealership {

	@Test
	void testNameIsNull() {
		DealershipGroup group = new DealershipGroup();

		assertThrows(IllegalArgumentException.class, () -> group.createOrGetDealership(null));
	}

	@Test
	void testNameIsEmpty() {
		DealershipGroup group = new DealershipGroup();

		assertThrows(IllegalArgumentException.class, () -> group.createOrGetDealership(""));
	}

	@Test
	void testDealershipIsCreated() {
		DealershipGroup group = new DealershipGroup();
		Dealership dealer = group.createOrGetDealership("Ted's Cars");

		assertEquals("Ted's Cars", dealer.getName());
	}

	@Test
	void testDealershipAlreadyExist() {
		DealershipGroup group = new DealershipGroup();
		group.addDealership("Ted's Cars");
		Dealership dealer = group.createOrGetDealership("Ted's Cars");

		assertEquals("Ted's Cars", dealer.getName());
	}
}
